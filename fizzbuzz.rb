module Fizzbuzz
	def self.take(n)
		(1..n)
			.map { |x|
				result = fizz(x) + buzz(x)
				result.empty? ? x.to_s : result.capitalize!
			}
	end

	private

	def self.fizz(n)
		n % 3 == 0 ? 'fizz' : ''
	end

	def self.buzz(n)
		n % 5 == 0 ? 'buzz' : ''
	end
end

puts Fizzbuzz.take(25)
